import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './Navbar.css'
export class Navbar extends Component {
    render() {
        return (
            <div>
                <nav className='navbar navbar-expand-md navbar-dark  Navbar'>
                    <p className='navbar-brand'> Welcome to our Sign Up page</p>
                    <ul className='navbar-nav'>
                        <li className='nav-item'>
                            <p className='nav-link'>Login</p>
                        </li>
                        <li className='nav-item'>
                            <p className='nav-link'>Home</p>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default Navbar