import './App.css'
import 'bootstrap'
import Form from './Components/Form/Form'
import Navbar from './Components/Navbar/Navbar'



function App() {
  return (
    <div className="App">
      <Navbar />
      <Form />
    </div>
  )
}

export default App
