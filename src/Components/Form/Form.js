import React, { Component } from 'react'
import validator from 'validator'

import './Form.css'

export class Form extends Component {
    constructor(props) {
        super(props)

        this.state = {
            firstname: "",
            lastname: "",
            age: "",
            gender: "",
            role: "",
            email: "",
            password: "",
            confirmPassword: "",
            agreedTo: false,
            hasErrors: false,
            formValidated: false,
        }
    }

    handleSubmit = (event) => {
        console.log(this.state)
        event.preventDefault()
        this.validateform()
    }

    validatePassword = (password) => {
        let passwordErrors = {}

        const upperCaseLetter = /[A-Z]/
        const lowerCaseLetter = /[a-z]/
        const specialCharacter = /[!@#$%^&*(),.?":{}|<>]/
        const digit = /[0-9]/

        if (!upperCaseLetter.test(password)) {
            passwordErrors.upperCaseLetter = "Atleast one upper case letter"
        }
        if (!lowerCaseLetter.test(password)) {
            passwordErrors.lowerCaseLetter = " Atleast one lower case letter"
        }
        if (!specialCharacter.test(password)) {
            passwordErrors.specialCharacter = " Atleast one special character"
        }
        if (!digit.test(password)) {
            passwordErrors.digit = " Atleast one digit"
        }
        if (password.length < 8) {
            passwordErrors.length = "Minimum 8 characters"
        
        }

        return passwordErrors

    }

    validateform = () => {
        let errors = {}

        if (!validator.isAlpha(this.state.firstname)) {
            errors.firstname = "Firstname should contain only letters"
        } else if (validator.isEmpty(this.state.firstname)) {
            errors.firstname = "Firstname cannot be empty"
        }

        if (!validator.isAlpha(this.state.lastname)) {
            errors.lastname = "Lastname should contain only letters"
        } else if (validator.isEmpty(this.state.lastname)) {
            errors.lastname = "Lastname cannot be empty"
        }

        if (isNaN(this.state.age)) {
            errors.age = "Age should be a number"
        } else if (this.state.age < 14 || this.state.age > 120) {
            errors.age = "Age should be in range of 14 to 120"
        }

        if (validator.isEmpty(this.state.gender)) {
            errors.gender = "Please select a value"
        }

        if (validator.isEmpty(this.state.role)) {
            errors.role = "Please select a role!"
        }
        if (!validator.isEmail(this.state.email)) {
            errors.email = "Please enter a valid email"
        }

        errors.password=this.validatePassword(this.state.password)
       



        if (!validator.matches(this.state.confirmPassword, this.state.password)) {
            errors.confirmPassword = "Password does not match! Try again?"
        }
        if (!this.state.agreedTo) {
            errors.terms = "Please agree to our terms"
        }
        console.log(JSON.stringify(errors))
        if (JSON.stringify(errors) !== `{"password":{}}`) {
            this.setState({
                hasErrors: true,
                errors: errors
            })
        } else {
            this.setState({
                hasErrors: false,
                errors: null,
                formValidated: true,
            })
        }


    }
    render() {
        return (
            <div className='container'>
                <form className={this.state.formValidated ? 'd-none' : 'card Card'}>
                    <div className='row gy-3'>

                        {/* name */}
                        <div className='form-group col-md-6 col-sm-12'>
                            <label className='form-label' htmlFor="firstname"><i className="fa-solid fa-user"> </i> First name</label>
                            <input
                                className='form-control'
                                type="text"
                                id="firstname"
                                placeholder='Enter your First name'
                                onInputCapture={(event) => {
                                    this.setState({
                                        firstname: event.target.value,
                                        errors: {
                                            ...this.state.errors,
                                            firstname: '',
                                        }
                                    })

                                }}
                            />
                            <span className='text-danger'>{this.state.hasErrors && this.state.errors.firstname}</span>

                        </div>

                        <div className='form-group col-md-6 col-sm-12'>
                            <label className='form-label' htmlFor="lastname">Last Name</label>
                            <input
                                className='form-control'
                                type="text"
                                id='lastname'
                                placeholder='Enter your last name'
                                onChange={(event) => {
                                    this.setState({
                                        lastname: event.target.value,
                                        errors: {
                                            ...this.state.errors,
                                            lastname: '',
                                        }
                                    })

                                }}
                            />
                            <span className='text-danger'>{this.state.hasErrors && this.state.errors.lastname}</span>

                        </div>
                        {/* Age */}
                        <div className='form-group col-md-6 col-sm-12'>
                            <label className='form-label' htmlFor='age'><i className="fa-solid fa-person-cane"></i> Age</label>
                            <input
                                className='form-control'
                                id="age"
                                type="number"
                                min="3"
                                max="200"
                                placeholder='Please enter your age'

                                onChange={(event) => {
                                    this.setState({
                                        age: Math.abs(event.target.value),
                                        errors: {
                                            ...this.state.errors,
                                            age: '',
                                        }
                                    })

                                    event.target.value = Math.abs(event.target.value)

                                }}
                            />
                            <span className='text-danger'>{this.state.hasErrors && this.state.errors.age}</span>
                        </div>

                        {/* Gender */}
                        <div className='form-group col-md-6 col-sm-12'>
                            <label className='form-label' htmlFor='gender'> <i className="fa-solid fa-venus-mars"></i> Gender</label>
                            <select
                                className='form-select'
                                id="gender"
                                onChange={(event) => {
                                    this.setState({
                                        gender: event.target.value,
                                        errors: {
                                            ...this.state.errors,
                                            gender: '',
                                        }
                                    })

                                }}
                            >
                                <option value="">Select </option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                                <option value="not specified">Prefer Not to say</option>

                            </select>
                            <span className='text-danger'>{this.state.hasErrors && this.state.errors.gender}</span>
                        </div>

                        {/* Role */}
                        <div className='form-group col-12'>
                            <label htmlFor="role"> <i className="fa-solid fa-briefcase"></i> Role</label>
                            <select
                                className="form-select"
                                id="role"
                                onChange={(event) => {
                                    this.setState({
                                        role: event.target.value,
                                        errors: {
                                            ...this.state.errors,
                                            role: '',
                                        }
                                    })


                                }}
                            >
                                <option value="">Select your role</option>
                                <option value="developer">Developer</option>
                                <option value="senior developer">Senior Developer</option>
                                <option value="lead engineer">Lead Engineer</option>
                                <option value="CTO">CTO</option>

                            </select>
                            <span className='text-danger'>{this.state.hasErrors && this.state.errors.role}</span>

                        </div>

                        {/* Email */}
                        <div className='form-group col-12'>
                            <label htmlFor='email'> <i className="fa-solid fa-envelope"></i> Email</label>
                            <input
                                className='form-control'
                                id="email"
                                type="email"
                                placeholder='Enter your email id'
                                onChange={(event) => {
                                    this.setState({
                                        email: event.target.value,
                                        errors: {
                                            ...this.state.errors,
                                            email: '',
                                        }
                                    })


                                }}
                            />

                            <span className='text-danger'>{this.state.hasErrors && this.state.errors.email}</span>
                        </div>

                        {/* Password */}

                        <div className='form-group col-12'>
                            <label htmlFor='password'> <i className="fa-solid fa-lock"></i> Create a password</label>
                            <input
                                className='form-control'
                                type="password"
                                id="password"

                                placeholder='Create a new password with atleast one special character, one upper case character, one digit and one lowercase character'
                                onChange={(event) => {
                                    this.setState({
                                        password: event.target.value,
                                        errors: {
                                            ...this.state.errors,
                                            password: this.validatePassword(event.target.value),

                                        }
                                    })


                                }}
                            />
                            <span className='text-danger'>{
                                this.state.hasErrors && Object.entries(this.state.errors.password).map(([key,error]) => {
                                    return <span className='d-block' key={key}> {error}</span>
                                })
                            }</span>

                        </div>

                        <div className='form-group col-12'>
                            <label htmlFor='confirmPassword'> <i className="fa-solid fa-key"></i> Confirm password</label>
                            <input
                                className='form-control'
                                type="password"
                                id="confirmPassword"
                                placeholder='Enter your password again'
                                onChange={(event) => {
                                    this.setState({
                                        confirmPassword: event.target.value,
                                        errors: {
                                            ...this.state.errors,
                                            confirmPassword: '',
                                        }
                                    })


                                }}
                            />
                            <span className='text-danger'>{this.state.hasErrors && this.state.errors.confirmPassword}</span>
                        </div>

                        {/* Agree tos */}
                        <div className='form-group'>
                            <input
                                className='form-check-input m-1'
                                type="checkbox"
                                onChange={(event) => {
                                    this.setState({
                                        agreedTo: !this.state.agreedTo,
                                        errors: {
                                            ...this.state.errors,
                                            terms: '',
                                        }
                                    })


                                }}
                            />
                            <label>I agree to  <span className='text-decoration-underline text-success'>terms and conditions </span>and <span className='text-decoration-underline text-success'>privacy policies</span> </label>
                            <p className='text-danger'>{this.state.hasErrors && this.state.errors.terms}</p>
                        </div>

                        <button
                            onClick={(event) => {
                                this.handleSubmit(event)
                            }}
                            className='btn btn-primary col-10 col-md-4 mx-auto gy-4 mb-2'
                        > Submit</button>


                    </div>


                </form>

                <div className={this.state.formValidated ? 'd-block' : 'd-none'}>
                    <h2 className='text-center'> Success! Your account has been created!</h2>
                </div>
            </div>
        )
    }
}

export default Form